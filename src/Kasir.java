import java.util.Scanner;

public class Kasir {
    private Meja[] daftarMeja;
    private Menu[] daftarMenu;

    public Kasir() {
        daftarMeja = new Meja[10];
        for (int i = 0; i < 10; i++) {
            daftarMeja[i] = new Meja(i + 1);
        }

        daftarMenu = new Menu[5];
        daftarMenu[0] = new Menu("Nasi Goreng", 15000);
        daftarMenu[1] = new Menu("Mi Goreng", 15000);
        daftarMenu[2] = new Menu("Capcay", 20000);
        daftarMenu[3] = new Menu("Bihun Goreng", 17000);
        daftarMenu[4] = new Menu("Ayam Koloke", 25000);
    }

    // digunakan untuk menampilkan daftar meja beserta keterangan ketersediaannya
    // gunakan method isKosong pada class Kasir agar lebih mudah
    public void tampilkanDaftarMeja() {
        //EDIT DISINI

        System.out.println("Daftar Meja:");

        //Menggunakan for untuk melakukan perulangan sebanyak jumlah meja yang ada
        for (Meja meja : daftarMeja) {

            //Menggunakan string berupa status untuk mengisi apakah meja kosong atau sudah terisi
            String status = meja.isKosong() ? "(Kosong)" : "(Terisi oleh pelanggan " + meja.getPelanggan().getNama() +")";

            //Menampilkan meja beserta statusnya
            System.out.println("Meja " + meja.getNomorMeja() + " : " + status);
        }
    }

    // untuk menambahkan pelanggan pada meja tertentu
    // jika meja kosong tambahkan pelanggan pada meja tersebut
    // jika tidak buatlah keterangan bahwa meja sudah ada pelanggan
    public void tambahPelanggan(int nomorMeja, Pelanggan pelanggan) {
        //EDIT DISINI

        //Memilih meja
        Meja meja = daftarMeja[nomorMeja-1];

        /*
        Mengecek apakah terdapat pelanggan pada meja
        Jika meja kosong maka pelanggan dapat ditambahkan
        */
        if (meja.isKosong()) {
            meja.setPelanggan(pelanggan);
        } 

        //Jika sudah terdapat pelanggan pada meja tersebut, akan menampilkan pesan meja sudah terisi
        else {
            System.out.println("Meja sudah terisi");
        }
    }
    
    // menambah pesanan menu pada nomor meja
    // jika menu tidak ada dalam daftar maka tampilkan "Menu is null" 
    public void tambahPesanan(int nomorMeja, Menu menu) {
        //EDIT DISINI

        //Mendeklarasi local bolean agar dapat digunakan didalam perulangan (for)
        boolean bolean = false;
        Meja meja = daftarMeja[nomorMeja-1];

        //Menggunakan for untuk melakukan perulangan sebanyak daftar menu yang ada
        for (Menu menuList : daftarMenu) {

            //Menggunakan if serta bolean untuk mengecek apakah pesanan terdapat pada menu
            if (menuList == menu) {
                bolean = true;
                break;
            }
        }

        /*
        Menggunakan if untuk mengecek status dari boolean bolean
        Jika pesanan tersedia di menu (bolean = true), pesanan akan ditambahkan
        */
        if (bolean) {
            meja.setMenu(menu);
        } 

        //Pesanan tidak tersedia di menu (bolean = false)
        else {
            System.out.println("Menu is null");
        }
    }

    // Menghapus pelanggan 
    public void hapusPelanggan(int nomorMeja) {
        //EDIT DISINI

        //Memilih nomor meja
        Meja meja = daftarMeja[nomorMeja - 1];

        /*
        Mengecek apakah terdapat pelanggan pada meja tersebut
        Jika meja kosong maka maka akan menampilkan pesan meja sudah kosong
        */
        if (meja.isKosong()) {
            System.out.println("Meja sudah kosong");
        } 
        
        //Jika terdapat pelanggan, pelanggan akan dihilangkan dengan mengisikan nilai pelanggan dengan null
        else {
            meja.setPelanggan(null);
        }
    }

    public int hitungHargaPesanan(int nomorMeja) {
        int totalHarga = 0;
        Meja meja = daftarMeja[nomorMeja - 1];
        Pelanggan pelanggan = meja.getPelanggan();
        Menu[] menu = meja.getMenu();
        if (pelanggan != null && menu != null && menu.length > 0) {
            for (int i = 0; i < menu.length; i++) {
                if (menu[i] != null) {
                    totalHarga += menu[i].getHarga();
                }
            }
            return totalHarga;
        }
        return totalHarga;
    }

    public void tampilkanPesanan(int nomorMeja) {
        Meja meja = daftarMeja[nomorMeja - 1];
        Pelanggan pelanggan = meja.getPelanggan();
        Menu[] menu = meja.getMenu();
        if (pelanggan != null && menu != null && menu.length > 0) {
            System.out.println("Pesanan untuk Meja " + nomorMeja);
            for (int i = 0; i < menu.length; i++) {
                if (menu[i] != null) {
                    System.out.println(menu[i].getNama() + " - " + menu[i].getHarga());
                }
            }
        } 
        else {
            System.out.println("Meja " + nomorMeja + " tidak memiliki pesanan");
        }
    }    

    public void tampilkanDaftarMenu() {
        System.out.println("Daftar Menu:");
        System.out.println("1. Nasi Goreng - Rp15.000");
        System.out.println("2. Mi Goreng - Rp15.000");
        System.out.println("3. Capcay - Rp20.000");
        System.out.println("4. Bihun Goreng - Rp17.000");
        System.out.println("5. Ayam Koloke - Rp25.000");
        System.out.println("6. Simpan");
        System.out.println();
    }

    public void tampilkanDaftarFitur() {
        System.out.println("1. Tampilkan daftar meja");
        System.out.println("2. Tambah pelanggan");
        System.out.println("3. Tambah pesanan");
        System.out.println("4. Hapus pelanggan");
        System.out.println("5. Hitung harga pesanan");
        System.out.println("6. Tampilkan pesanan di meja");
        System.out.println("0. Keluar");
    }

    public void jalankan() {
        Scanner scanner = new Scanner(System.in);
        int pilihan = -1;
        while (pilihan != 0) {
            tampilkanDaftarFitur();
            System.out.print("Masukkan pilihan: ");
            pilihan = scanner.nextInt();
            scanner.nextLine();  
            switch (pilihan) {
                case 1:
                    // menampilkan daftar meja dengan method yang sudah ada
                    //EDIT DISINI

                    //Memanggil method untuk menampilkan daftar meja beserta statusnya
                    tampilkanDaftarMeja();
                    break;
                case 2:
                    // tampilkan pesan untuk input nomor meja dan nama pelanggan untuk digunakan pada method
                    // jangan lupa instansiasi Pelanggan dengan nama pelanggan sesuai input
                    // EDIT DISINI

                    //Menginputkan nomor meja yang akan dipilih
                    System.out.print("Masukkan nomor meja: ");
                    int nomorMeja = scanner.nextInt();
                    scanner.nextLine();

                    //Menginputkan nama pelanggan
                    System.out.print("Masukkan nama pelanggan: ");
                    String namaPelanggan = scanner.nextLine();

                    //Mendeklarasi pelanggan dengan memanggil class pelanggan
                    Pelanggan pelanggan = new Pelanggan(namaPelanggan);

                    //Menambahkan pelanggan ke meja yang sudah dipilih
                    tambahPelanggan(nomorMeja, pelanggan);
                    break;
                case 3:
                    boolean stopLoop = false;
                    System.out.print("Masukkan nomor meja: ");
                    int nomorMejaPesan = scanner.nextInt();
                    Boolean meja = daftarMeja[nomorMejaPesan - 1].isKosong();
                    scanner.nextLine();  
                    if (!meja) {
                        tampilkanDaftarMenu();
                        while (!stopLoop) {
                            System.out.print("Masukkan nomor menu: ");
                            int nomorMenuPesan = scanner.nextInt();
                            scanner.nextLine();  
                            switch (nomorMenuPesan) {
                                case 1:
                                    tambahPesanan(nomorMejaPesan, daftarMenu[0]);
                                    break;
                                case 2:
                                    tambahPesanan(nomorMejaPesan, daftarMenu[1]);
                                    break;
                                case 3:
                                    tambahPesanan(nomorMejaPesan, daftarMenu[2]);
                                    break;
                                case 4:
                                    tambahPesanan(nomorMejaPesan, daftarMenu[3]);
                                    break;
                                case 5:
                                    tambahPesanan(nomorMejaPesan, daftarMenu[4]);
                                    break;
                                case 6:
                                    stopLoop = true;
                                    break;
                                default:
                                    System.out.println("Nomor menu tidak valid");
                                    break;
                            }
                        }
                    }
                    else {
                        System.out.println("Meja tidak ada pelanggan");
                    }
                    break;
                case 4:
                    // untuk menghapus pelanggan pada meja tertentu
                    // tampilkan pesan untuk memasukkan nomor meja yang akan dihapus untuk digunakan pada method hapusPelanggan()
                    // EDIT DISINI

                    //Menginputkan nomor meja yang akan dihapus pelanggannya
                    System.out.print("Masukkan nomor meja: ");
                    int nomorMeja4 = scanner.nextInt();

                    //menghapus pelanggan pada meja tersebut
                    hapusPelanggan(nomorMeja4);
                    break;
                case 5:
                    // Untuk melihat total harga pesanan pada meja tertentu
                    // tampilkan pesan untuk memasukkan nomor meja 
                    // jangan lupa membedakan keluaran apabila pelanggan belum memesan apapun / total harga 0
                    // EDIT DISINI

                    //Menginputkan nomor meja yang akan dipilih
                    System.out.print("Masukkan nomor meja: ");
                    int nomorMeja5 = scanner.nextInt();

                    /*
                    Mendeklarasikan local variable agar dapat digunakan dalam if else
                    Mengisikan nilai harga total kedalam variabel local
                    */
                    int totalHarga = hitungHargaPesanan(nomorMeja5);

                    //Menggunakan if untuk memastikan bahwa meja memiliki pesanan
                    if (totalHarga==0) {
                        System.out.printf("Meja %d tidak memiliki pesanan\n",nomorMeja5);
                    } 

                    //Jika meja memiliki pesanan, total harga akan dihitung dan ditampilkan
                    else {
                        System.out.println("Harga pesanan di meja "+nomorMeja5+" adalah "+totalHarga);
                    }
                    break;
                case 6:
                    // untuk melihat pesanan pada meja tertentu
                    // tampilkan pesan untuk memasukkan nomor meja 
                    // EDIT DISINI

                    //Menginputkan nomor meja yang akan dipilih
                    System.out.print("Masukkan nomor meja: ");
                    int nomorMeja6 = scanner.nextInt();
                    scanner.nextLine();
                    Meja meja6 = daftarMeja[nomorMeja6 - 1];
                    Menu[] menuPadaMeja = daftarMeja[nomorMeja6].getMenu();

                    //Menggunakan if untuk menampilkan output pada meja yang kosong
                    if (meja6.isKosong()) {
                        System.out.printf("Meja %d tidak memiliki pesanan\n",nomorMeja6);
                    } else {
                        tampilkanPesanan(nomorMeja6);
                    }

                    //Menggunakan if untuk menampilkan output pada meja yang terdapat pelanggan tetapi belum memesan
                    if (menuPadaMeja[0]==null) {
                        System.out.printf("Meja %d tidak memiliki pesanan\n",nomorMeja6);
                    }
                    break;

                case 0:
                    System.out.println("Terima kasih telah menggunakan aplikasi kasir restoran!");
                    break;
                default:
                System.out.println("Pilihan tidak valid");
                break;
        }
        System.out.println();
    }
    scanner.close();
    }
}